trace.enabled = true
trace.raw = true
trace.file = 'trace.txt'
trace.fields = 'task_id,name,realtime,%cpu,%mem,peak_rss,peak_vmem'
